import React, { PropTypes } from 'react';

import BoardCell from './BoardCell';

const styles = {
  board: {
    width: 300,
    listStyleType: 'none',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    padding: 0,
  },
};

const BoardGrid = ({ grid, handleNewMark, isWinnerMark, getPlayerSymbol }) => (
  <ul style={styles.board}>
    {grid.map((rows, rowColumn) =>
      rows.map((row, rowIndex) => (
        <BoardCell
          key={`row-${rowIndex}-${rowColumn}`}
          column={rowColumn}
          value={row}
          index={rowIndex}
          handleNewMark={handleNewMark}
          isWinnerMark={isWinnerMark}
          getPlayerSymbol={getPlayerSymbol}
        />
      ))
    )}
  </ul>
);

BoardGrid.propTypes = {

  grid: PropTypes.arrayOf(
    PropTypes.arrayOf(
      PropTypes.number
    ).isRequired
  ).isRequired,

  handleNewMark: PropTypes.func.isRequired,

  isWinnerMark: PropTypes.func.isRequired,

  getPlayerSymbol: PropTypes.func.isRequired,
};

export default BoardGrid;
