import React, { PropTypes } from 'react';
import { ModalContainer, ModalDialog } from 'react-modal-dialog';

const Modal = ({ show, children, onClose }) => (
  show && (
    <ModalContainer onClose={onClose}>
      <ModalDialog onClose={onClose}>
        {children}
      </ModalDialog>
    </ModalContainer>
  )
);

Modal.propTypes = {

  show: PropTypes.bool.isRequired,

  children: PropTypes.node,

  onClose: PropTypes.func,
};

export default Modal;
