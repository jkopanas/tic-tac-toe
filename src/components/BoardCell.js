import React, { PropTypes } from 'react';
import { css } from 'glamor';

import Icon from './Icon';

const styles = {
  boardSpace: css({
    fontSize: 40,
    width: 95,
    height: 95,
    border: '1px solid indigo',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5,

  }),
  winnerBoardCell: css({
    color: '#0291E8',
  }),
};

const SYMBOLS = {
  X: 'times',
  O: 'circle-o',
};

const BoardCell = ({ column, index, value, handleNewMark, isWinnerMark, getPlayerSymbol }) => {
  const icon = (value) ? SYMBOLS[getPlayerSymbol(value)] : null;

  const containerStyle = (isWinnerMark(column, index)) ? {
    ...styles.boardSpace,
    ...styles.winnerBoardCell,
  } : styles.boardSpace;

  return (
    <li
      {...containerStyle}
      onClick={() => handleNewMark(column, index)}
    >
      {(value !== null) ?
        <Icon name={icon} style={icon == 'times' ? {color: '#449E28'} : {color: '#28779E'} }/> :
        null
      }
    </li>
  );
};

BoardCell.propTypes = {

  column: PropTypes.number.isRequired,

  index: PropTypes.number.isRequired,

  value: PropTypes.number,

  handleNewMark: PropTypes.func.isRequired,

  isWinnerMark: PropTypes.func.isRequired,

  getPlayerSymbol: PropTypes.func.isRequired,
};

export default BoardCell;
