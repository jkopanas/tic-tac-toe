
const types = {
    NEW_MARK: 'NEW_MARK',
    GAME: {
        CHOOSE_START_PLAYER: 'GAME_CHOOSE_START_PLAYER',
        BOARD_VERIFY: 'GAME_BOARD_VERIFY',
        RESTART: 'GAME_RESTART',
        NEXT_PLAYER: 'GAME_NEXT_PLAYER',
    },
};


export const boardChoosePlayer = symbol => ({
  type: types.GAME.CHOOSE_START_PLAYER,
  symbol,
});

export const boardVerify = () => ({
  type: types.GAME.BOARD_VERIFY,
});

export const nextPlayer = () => ({
  type: types.GAME.NEXT_PLAYER,
});

export const newMark = (column, row) =>
  (dispatch) => {
    dispatch({
      type: types.NEW_MARK,
      column,
      row,
    });

    dispatch(boardVerify());

    return dispatch(nextPlayer());
  };

export const boardRestart = () => ({
  type: types.GAME.RESTART,
});
