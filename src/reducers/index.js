/**
 * Created by Giannis on 16/2/2017.
 */
import { combineReducers } from 'redux';

import game from './game';

const reducer = combineReducers({
  game,
});

export default reducer;
