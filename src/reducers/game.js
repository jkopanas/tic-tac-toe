/**
 * Created by Giannis on 16/2/2017.
 */

import cloneDeep from 'lodash.clonedeep';
import {
  BOARD_SIZE,
  generateBoardGrid,
  verifySpaces,
} from './gameHelpers';

const types = {
    NEW_MARK: 'NEW_MARK',
    GAME: {
        CHOOSE_START_PLAYER: 'GAME_CHOOSE_START_PLAYER',
        BOARD_VERIFY: 'GAME_BOARD_VERIFY',
        RESTART: 'GAME_RESTART',
        NEXT_PLAYER: 'GAME_NEXT_PLAYER',
    },
};

export const initialState = {
  boardGrid: generateBoardGrid(),
  curPlayer: 1,
  playersSymbols: {},
  winMark: {},
  playerWin: null,
  marksCount: 0,
  isTie: false,
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case types.GAME.CHOOSE_START_PLAYER: {
      return {
        ...state,
        playersSymbols: {
          1: action.symbol,
          2: (action.symbol === 'X') ? 'O' : 'X',
        },
      };
    }
    case types.NEW_MARK: {
      const { curPlayer } = state;
      const boardGrid = cloneDeep(state.boardGrid);

      const { column, row } = action;

      boardGrid[column][row] = curPlayer;

      return {
        ...state,
        boardGrid,
        marksCount: state.marksCount + 1,
      };
    }
    case types.GAME.BOARD_VERIFY: {
      const boardSize = BOARD_SIZE;
      const maxMarksCount = boardSize ** 2;

      const { curPlayer: playerWin, marksCount } = state;


      const boardGrid = cloneDeep(state.boardGrid);

      for (let i = 0; i < boardSize; i += 1) {
        // Check if user won horizontally

        if (verifySpaces(boardGrid[i][0], boardGrid[i][1], boardGrid[i][2])) {
          return {
            ...state,
            winMark: {
              [i]: [0, 1, 2],
            },
            playerWin,
          };
        }


        if (verifySpaces(boardGrid[0][i], boardGrid[1][i], boardGrid[2][i])) {
          return {
            ...state,
            winMark: {
              0: [i],
              1: [i],
              2: [i],
            },
            playerWin,
          };
        }
      }

      if (verifySpaces(boardGrid[0][0], boardGrid[1][1], boardGrid[2][2])) {
        return {
          ...state,
          winMark: {
            0: [0],
            1: [1],
            2: [2],
          },
          playerWin,
        };
      }

      if (verifySpaces(boardGrid[0][2], boardGrid[1][1], boardGrid[2][0])) {
        return {
          ...state,
          winMark: {
            0: [2],
            1: [1],
            2: [0],
          },
          playerWin,
        };
      }

      if (marksCount === maxMarksCount) {
        return {
          ...state,
          isTie: true,
        };
      }

      return state;
    }
    case types.GAME.NEXT_PLAYER: {
      return {
        ...state,
        curPlayer: ((state.curPlayer === 1) ? 2 : 1),
      };
    }
    case types.GAME.RESTART: {
      return {
        ...initialState,
        playersSymbols: state.playersSymbols,
      };
    }
    default: {
      return state;
    }
  }
};
