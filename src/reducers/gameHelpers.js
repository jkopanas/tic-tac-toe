/**
 * Created by Giannis on 16/2/2017.
 */

export const BOARD_SIZE = 3;

export const generateBoardGrid = () => {
  const boardSize = BOARD_SIZE;


  const boardGrid = new Array(boardSize);

  for (let i = 0; i < boardSize; i += 1) {
    const boardRows = new Array(boardSize);

    for (let j = 0; j < boardSize; j += 1) {
      boardRows[j] = null;
    }

    boardGrid[i] = boardRows;
  }

  return boardGrid;
};

export const verifySpaces = (firstSpace, secondSpace, thirdSpace) => (
  firstSpace !== null &&
  firstSpace === secondSpace &&
  firstSpace === thirdSpace
);
