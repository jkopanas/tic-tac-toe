/**
 * Created by Giannis on 16/2/2017.
 */

import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as boardActions from '../actions/boardActions';

import BoardGrid from '../components/BoardGrid';
import Button from '../components/Button';
import Modal from '../components/Modal';

const styles = {
  container: {
    marginTop: 20,
  },
  boardMainContainer: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  actionsMainContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
  modalactionsMainContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
  pageTitle: {
    borderBottom: '1px solid #CCC',
    marginBottom: 15,
    padding: '0 40px 10px',
  },
  title: {
    fontSize: 18,
    textAlign: 'center',
  },
  modalTitle: {
    fontSize: 30,
    marginTop: 0,
    textAlign: 'center',
  },
  modalSubtitle: {
    fontSize: 27,
    marginTop: 0,
    textAlign: 'center',
  },
  button: {
    margin: '0px 5px',
    padding: 5,
    fontSize: 14,
    minWidth: 60,
    background: '#449E28'
  },
  curPlayerContainer: {
    border: '1px solid #0291E8',
    padding: 10,
    borderRadius: 5,
  },
};

export class App extends Component {

  static propTypes = {

    boardGrid: PropTypes.arrayOf(
      PropTypes.arrayOf(
        PropTypes.number
      ).isRequired
    ).isRequired,

    curPlayer: PropTypes.number,

    winMark: PropTypes.object,

    playerWin: PropTypes.number,

    isTie: PropTypes.bool,

    playersSymbols: PropTypes.object,

    marksCount: PropTypes.number,
    actions: PropTypes.shape({

      boardRestart: PropTypes.func,

      boardChoosePlayer: PropTypes.func,

      newMark: PropTypes.func,
    }),
  };

  state = {
    showPlayerModal: true,
  };

  getPlayerSymbol = player => this.props.playersSymbols[player] || null;

  isWinnerMark = (column, index) => {
    const { winMark } = this.props;

    if (!winMark || !winMark[column]) {
      return false;
    }

    return (winMark[column].indexOf(index) !== -1);
  };

  handleNewMark = (column, row) => {
    const { boardGrid, playerWin } = this.props;

    if (boardGrid[column][row] || playerWin) {
      return null;
    }
    return this.props.actions.newMark(column, row);
  };

  choosePlayer = (player) => {
    this.props.actions.boardChoosePlayer(player);

    this.setState({
      showPlayerModal: false,
    });
  };

  render() {
    const { showPlayerModal } = this.state;
    const {
      boardGrid,
      curPlayer,
      winMark,
      playerWin,
      isTie,
      marksCount,
      actions: {
        boardRestart,
      },
    } = this.props;

      const showHide = {
          'display': showPlayerModal ? 'block' : 'none'
      };
      const showBoard = {
          'display': !showPlayerModal ? 'block' : 'none'
      };

    return (

      <div style={styles.container}>
        <div style={styles.boardMainContainer}>

          {!showPlayerModal && (
            <div style={styles.curPlayerContainer}>
              Current player: {curPlayer}
            </div>
          )}

          <div style={showHide}>
              Choose Symbol to Play:
            <Button
                name="symbol-X"
                icon="times"
                iconPosition="after"
                onClick={() => this.choosePlayer('X')}
                style={styles.button}
            />
            <Button
                name="symbol-O"
                icon="circle-o"
                iconPosition="after"
                onClick={() => this.choosePlayer('O')}
                style={styles.button}
            />
          </div>
          <div style={showBoard}>
          <BoardGrid
            grid={boardGrid}
            winMark={winMark}
            handleNewMark={this.handleNewMark}
            isWinnerMark={this.isWinnerMark}
            getPlayerSymbol={this.getPlayerSymbol}
          /></div>
        </div>

        {marksCount > 0 && (
          <div style={styles.actionsMainContainer}>
            <Button
              icon="refresh"
              onClick={() => boardRestart()}
              label="Restart Board"
              style={styles.button}
            />
          </div>
        )}

        <Modal
          show={isTie || !!playerWin}
          onClose={() => boardRestart()}
        >
          {isTie && (
            <div>
              <h1 style={styles.modalTitle}>It's a tie!</h1>
            </div>
          )}

          {playerWin && (
            <div>
              <h1 style={styles.modalTitle}>Player {playerWin} won !</h1>
            </div>
          )}

          <div style={styles.modalactionsMainContainer}>
            <Button
              name="restart"
              icon="play"
              onClick={() => boardRestart()}
              label="New"
            />
          </div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = ({ game }) => ({ ...game });

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(boardActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
