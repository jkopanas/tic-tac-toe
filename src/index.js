/**
 * Created by Giannis on 16/2/2017.
 */
import 'babel-polyfill';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';

import configureStore from './store/configureStore';

import ConnectApplication from './containers/App';

const rootEl = document.getElementById('root');

const store = configureStore();

render(
  <AppContainer>
    <Provider store={store}>
      <ConnectApplication />
    </Provider>
  </AppContainer>,
  rootEl
);