# Tic Tac Toe

Undefeatable tic tac toe (minimax algorithm) developed with ES6, React.js and Redux


## Run it yourself

1. Clone repository to local disk
1. In repository root directory, run:

  `npm install`

  `npm start`

1. Navigate to [http://localhost:8000](http://localhost:8000)
